title: EveryMatrix
file_name: EveryMatrix
canonical_path: /customers/everymatrix/
cover_image: /images/blogimages/everymatrix_cover_image.jpg
cover_title: |
  Learn how EveryMatrix wins big with GitLab
cover_description: |
  Full-service B2B gaming platform provider EveryMatrix wanted faster iterations and better control over open-source innovation
twitter_image: /images/blogimages/everymatrix_cover_image.jpg
twitter_text: Learn how EveryMatrix wins big with GitLab @EveryMatrix
customer_logo: /images/case_study_logos/logo.everymatrix.png
customer_logo_css_class: brand-logo-tall
customer_industry: Gaming
customer_location: Bucharest,Romania (HQ)
customer_solution: GitLab Self Managed Premium
customer_employees: |
  300 (developers)
customer_overview: |
  EveryMatrix's development and infrastructure leaders turned to GitLab Self Managed Premium for a DevOps platform
customer_challenge: |
  The rapid-pace of services roll-out for casino, sports and esports betting services called for frictionless, automated workflows. EveryMatrix tech leaders needed to adopt methods to handle multiple open-source tool installations and deployment pipelines, while still guaranteeing correct peer reviews and approval processes.  
key_benefits: >-

  
    Seamless support for peer review, dependable approval processes.  

  
    Automation with a single CICD platform.

  
    Transparency for diverse stakeholders helps foster open-source culture.

  
    Supports the open-source development model

  
    Robust Kubernetes integration


    Attracting top developer talent
customer_stats:
  - stat: 50%
    label: 50% users growth in 1 year (200 to 300)
  - stat:  42%
    label: 250k pipelines and growing (42% yearly growth)
  - stat:  25%
    label: 1700 projects with 25% yearly growth
customer_study_content:
  - title: the customer
    subtitle: EveryMatrix moves maps a plan for moving gaming services forward
    content: >-
  
       [EveryMatrix](https://everymatrix.com/) provides an API-based B2B product suite for casino, sports betting, payments and affiliate management in highly-regulated industry segments across the globe. Elements provided include delivery of content, betting odds, and scores, as well as transactional support for real-time settlements, analytics and risk management, all encompassed in a platform providing assured player protection. Work is supported by a dedicated infrastructure team that is responsible for governance and ensuring adherence to regional and local regulations. Services can be deployed as independent or integrated solutions. With live sports on hold during parts of the COVID-19 pandemic, the company successfully included more virtual sport events in its offerings. EveryMatrix solutions have evolved to cover more than 125 sports and virtual sports, comprising over 105,000 live events in early 2021.


  - title: the challenge
    subtitle:  Building a practical open-source services delivery platform for fast-paced sector
    content: >-
    

        As a global provider of advanced gaming services, EveryMatrix is focused on fast-paced, innovative solution development and dependable software deployment for users in a wide selection of countries and regions. Such lively progress is tempered in gaming, as elsewhere, by a need for security, high-availability and reliability that helps ensure trust of clients and their online customers. With a growing focus on APIs and Kubernetes, it became clear the development teams needed more streamlined and automated workflows. 


        General tooling advances and the need to install important add-ons underscored the requirement to adapt to tools flexibly and quickly. To effectively implement agile processes, EveryMatrix teams needed assurance that everything was in one place, that workflows were well-documented, and that front-line developers didn’t have to be versed in a different installation method for each tool they needed to use.  Programmer teams needed dependable peer review and approvals processes. As well, the system had to be capable of providing transparency to other stakeholders in the organization. CICD support that enabled such innovation was a must-have, and GitLab Premium was the choice.



  - title: the solution
    subtitle:  A unified GitLab workflow and one DevOps platform 
    content: >-


        GitLab Premium implementation meets EveryMatrix requirements for a single repository that reduces the complexity of working with multiple open-source frameworks. Integrated, flexible tooling supports teams’ initiatives to automate repetitive tasks and to deploy open-source software quickly and securely. Now, developers no longer need to change from tool to tool, managing multiple different installations, according to Rafael Campuzano, Group CTO, EveryMatrix. GitLab thus supports the company’s daily devOps-oriented project work by providing everything necessary in a single tool, with regularly updated functionality. 

      
        All business units have implemented GitLab’s container registry, Campuzano said, and all teams are able to set up rules for code pushes through the repository. Importantly, he added, GitLab has been implemented in such a way that different business units can effectively interact with one another, while sharing and integrating code based on teams’ individual priority cycles. “GitLab for us is a really important tool to achieve an internal open-source model,” said Campuzano. 


  - title: the results
    subtitle:  Trust is key for open-source model
    content: >-

        The new platform has become key to improvements in processes and workflows, and now supports more than 200 developers. “With GitLab, all approval processes and code review mechanisms provide exactly the features we need. It’s a perfect fit with our intentions,” said Teodor Coman, Frontend Technical Director, EveryMatrix. “GitLab CICD tools are the core of our operations.” 


        Teams can set up pipelines and manage deployments with much better control. Moreover, with GitLab, teams benefit from workflow approval and peer review capabilities. EveryMatrix team members give GitLab high marks for culture and adherence to open-source philosophy. Incidents and updates are handled in a transparent way. EveryMatrix has reaped huge benefits using GitLab; 50% user growth from 200 to 300 in just one year, 25% annual project growth and over 250K pipelines increasing over 42% annually. 


        “With GitLab, it’s not just a set of tools, it’s also the culture of the company. If there is an incident, we receive answers in a very transparent way. That’s important for our culture here, too,” said Rafael Campuzano. “We are dealing with gaming players. If players feel you are not transparent, they run away. Trust is key.”


   

        ## Learn more about GitLab solutions
    
  
        [Security with GitLab](/solutions/dev-sec-ops/)
    
  
        [CI with GitLab](/stages-devops-lifecycle/continuous-integration/)
    
  
        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: With GitLab, it’s not just a set of tools, it’s also the culture of the company. If there is an incident, we receive answers in a very transparent way. That’s important for our culture here, too. We are dealing with gaming players. If players feel you are not transparent, they run away. Trust is key.
    attribution: Rafael Campuzano
    attribution_title: Group CTO, EveryMatrix











