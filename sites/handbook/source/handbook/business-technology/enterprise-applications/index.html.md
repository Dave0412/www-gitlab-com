---
layout: handbook-page-toc
title: "Enterprise Applications Team"
description: "The Enterprise Applications Team implements and supports specialized applications that support our business processes within GitLab."
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# <i class="fas fa-users" id="biz-tech-icons"></i> Who We Are


The **Enterprise Applications Team** implements and supports specialized applications that support our business processes within GitLab.

We are directly responsible for all of GitLab's finance systems and Enterprise Applications Integrations.  We build and extend these applications to support the processes of our business partners and rationalize our application landscape to ensure it is optimized for efficiency and spend.

Our team ensures the availability of these applications and integrations through monitoring and alerting. These internal-facing applications include a multitude of different applications and environments, including Zuora, Adaptive Planning, Netsuite, Expensify, etc.  We are also responsible for the IT Audit and Compliance function to ensure we pass SOX Audit for our IT General Controls (ITGC).

Our Enterprise Applications team is made up of a combination of roles to best support the services we offer. Learn more about each by clicking on the tiles:


<div class="flex-row" markdown="0" style="height:80px">
  <a href="/handbook/business-technology/enterprise-applications/bsa/" class="btn btn-purple-inv" style="width:25%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Business Systems Analysts</a>
  <a href="/handbook/business-technology/it-compliance/" class="btn btn-purple-inv" style="width:25%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">IT Compliance</a>
  <a href="/handbook/business-technology/enterprise-applications/financeops/" class="btn btn-purple-inv" style="width:25%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Finance Operations</a>
  <a href="/handbook/business-technology/enterprise-applications/integrations/" class="btn btn-purple-inv" style="width:25%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Integrations Engineering</a>
</div>

# <i class="fas fa-bullseye" id="biz-tech-icons"></i> Our Vision

- To enable end to end business processes within the enterprise applications that seamlessly hand off to each other and ensure it provides a great user experience to our business partners
- Ensure data integrity between systems  and security of that data
- Constantly iterate to simplify and ensure processes are efficient and automated as much as possible..
- Leveraging out of the box best practices as much as possible. We buying and extend applications where we don't see building them as GitLabs core engineering competency
- IT Audit and Compliance - Ensuring that all customer / business data is secure and can pass key audits for attestations and compliance with SOX, SOC, etc.


# <i class="fas fa-bullhorn" id="biz-tech-icons"></i> Services We Offer

### Business Process Improvements
* Being business process first, means that the Enterprise Applications team will firm up requirements, use cases, and process flows as we implement systems, enhance them or deliver fixes.   Learn more [here](https://about.gitlab.com/handbook/business-technology/enterprise-applications/bsa/).

### Application Evaluations & Implementations 
*  We provide templates for vendor evaluations, can help write and review your user stories, and are happy to participate in tool evaluations that integrate with other applications or intersect with multiple departments. Once an application is selected, our team will align with vendor teams to implement Enterprise Applications that are coming on board. We follow a process that ensures we keep multiple groups aligned as we iterate to get the systems up quickly and efficiently. Learn more [here](https://about.gitlab.com/handbook/business-technology/enterprise-applications/bsa/#system-implementations).

### Finance Systems Administration
*  Enterprise Applications supports all of the core finance systems with experienced admins that streamline and enhance current processes, turn on new features, and improve end to end process cycle time.

### Integrations Engineering
* Our integration team manages all of the integrations between Enterprise Applications at GitLab.  Focusing on building out failover, redundant and auditable integrations that are constantly monitored. Learn more [here](https://about.gitlab.com/handbook/business-technology/enterprise-applications/integrations/).

### IT Audit and Compliance
* Focusing on operationalizing and optimizing the Information Technology General Compute Controls (ITGCs) for GitLab.  This is a critical step to supporting our security posture and meeting SOX compliance and becoming a public company. Learn more in our [IT Compliance handbook page](https://about.gitlab.com/handbook/business-technology/it-compliance/).

### Project Retrospectives
* We can host your project retrospective. Please open an issue in the [Business Technology tracker](https://gitlab.com/gitlab-com/business-technology/business-technology/-/issues/new)

### Architectural Troubleshooting

* We have high level views of the enterprise application ecosystem and can help troubleshoot where a business process has broken down or a system flow is not working as expected. You can open an issue in the [Business Technology Tracker](https://gitlab.com/gitlab-com/business-technology/business-technology/-/issues/new).


# <i class="fas fa-building" id="biz-tech-icons"></i> Results We Delivered in FY22

### New Application Implementations
- [Zuora Revenue Implementation](https://gitlab.com/groups/gitlab-com/business-technology/-/epics/76)
- [Adaptive Insights Implementation](https://gitlab.com/gitlab-com/business-technology/enterprise-apps/financeops/finance-systems/-/wikis/2.-Adaptive-Insights)
- [Coupa P2P Implementation](https://gitlab.com/groups/gitlab-com/business-technology/enterprise-apps/-/epics/168)
- [DocuSign Implementation](https://gitlab.com/groups/gitlab-com/business-technology/-/epics/215)

### Other Initiatives
- [Zuora CPQ & 360 upgrade to latest version](https://gitlab.com/groups/gitlab-com/business-technology/enterprise-apps/-/epics/75)


# <i class="fas fa-file-alt" id="biz-tech-icons"></i>  Templates We Use
*  [Rollout Plan](https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=public-rollout-plan)
*  [Change Management: Third Party Applications Changes](https://gitlab.com/gitlab-com/business-technology/change-management/issues/new?issuable_template=Third%20Party%20Change%20Management)
*  [Change Management: Internal Tool Changes](https://gitlab.com/gitlab-com/business-technology/change-management/issues/new?issuable_template=Internal_Change_Management)
*  [Software application selection: Request for Proposal](https://docs.google.com/document/d/1_Q2b5opYUQ9TlGmF2vOJ6anu0spVFMkNO6YCR4UjYXM/edit?usp=sharing)
*  [Software application selection: User Stories](https://docs.google.com/spreadsheets/d/1c1R0pqKr8YwXXATzFVEUaofF2luNrHbmcNkKAWisebs/edit?usp=sharing)

# <i class="fas fa-database" id="biz-tech-icons"></i>  Applications We Own

1. Zuora
1. Z-Revenue (RevPro)
1. Netsuite
1. Coupa
1. Tipalti
1. Expensify
1. Stripe
1. TripActions
1. Avalara
1. CaptivateIQ
1. Workiva
1. FloQast
1. Adaptive Planning
1. Xactly
1. Mavenlink
1. EdCast
1. DocuSign


# <i class="fas fa-book" id="biz-tech-icons"></i>  Documentation We Support
- [Enterprise Application guides for internal training](./guides)
- [Tech Stack](/handbook/business-technology/tech-stack-applications/)
- [Change Management](/handbook/business-technology/change-management/)

For process flows, integration diagrams and more, check out our Business Systems Documentation pages:

- [Quote to cash](/handbook/business-technology/enterprise-applications/quote-to-cash)


# <i class="fas fa-tasks" id="biz-tech-icons"></i>  Our Process

### Types of Support

1. Access Request or change in access: [Queue](https://gitlab.com/groups/gitlab-com/-/boards/1765444?&label_name[]=FinSys%20-%20Access%20Request).
    Submit [issue](/handbook/business-technology/team-member-enablement/onboarding-access-requests/access-requests/).
1. Breaks, bugs and incidents related to a system.
    Submit [issue](https://gitlab.com/gitlab-com/business-technology/enterprise-apps/financeops/finance-systems/-/issues/new).
1. Enhancement Request for a system.
    Submit [issue](https://gitlab.com/gitlab-com/business-technology/enterprise-apps/financeops/finance-systems/-/issues/new).
1. Other and questions.
    Submit [issue](https://gitlab.com/gitlab-com/business-technology/enterprise-apps/financeops/finance-systems/-/issues/new).

### Issue Process
1. [Issue](https://gitlab.com/gitlab-com/business-technology/enterprise-apps/financeops/finance-systems/-/issues/new) submitted with request
1. Request is approved by technical owner and business owner (as necessary).
    ([Approvals Queue](https://gitlab.com/groups/gitlab-com/-/boards/1774935))
1. Change pushed to sandbox/dev environment
1. Change validated
1. Change deployed to production environment

### Issue Labels
Every issue will have a scope tracking tag showing the current status:
- `BT:Backlog` - Unless a due date is indicated or urgency specified, non-access related issues will go into the backlog and prioritized bi-weekly
- `BT:To Do` - Team will look at the issue within a week of submitting
- `BT:In Progress` - Team is currently actively working on scoping out and gathering requirements
- `BT:Done` - Completed

We also use labels to identify the responsible group within Enterprise Applications:
- `BSA` - business systems analysts issue
- `BT Finance Systems` - finance admin issue
- `BT Integrations:Kanban` - integrations team issue
- `IT Compliance` - IT compliance team

# <i class="fas fa-headset" id="biz-tech-icons"></i> Contact Us
### Slack Channels
- `#enterprise-apps`
- `#business-technology`
- `#bt-finance-operations`
- `#financesystems_help`
- `#bt-integrations`

### Open an Issue
<div class="flex-row" markdown="0" style="height:80px">
  <a href="https://gitlab.com/gitlab-com/business-technology/business-technology/-/issues/new" class="btn btn-purple-inv" style="width:33%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Business Systems Analysts</a>
  <a href="https://gitlab.com/gitlab-com/business-technology/enterprise-apps/integrations/issue-tracker/-/issues/new" class="btn btn-purple-inv" style="width:33%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Integrations Engineering</a>
  <a href="https://gitlab.com/gitlab-com/business-technology/enterprise-apps/financeops/finance-systems" class="btn btn-purple-inv" style="width:33%;height:100%;margin:1px;display:flex;justify-content:center;align-items:center;">Finance Systems</a>
