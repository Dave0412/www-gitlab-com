---
layout: job_family_page
title: Frontend Engineer - Marketing
description: "Frontend Engineers on the Marketing team work on our marketing site. They work closely with product marketing, content marketing, and other members of the Marketing team."
---
 
Frontend Engineers on the Marketing team work on our marketing site: about.GitLab.com. They work closely with product marketing, content marketing, and other members of the Marketing team. They are a part of the [Digital Experience team](/handbook/marketing/inbound-marketing/digital-experience/), and report to the [Senior Manager, Digital Experience](/job-families/marketing/marketing-web-developer-designer/#senior-manager-digital-experience).
 
## Responsibilities
* Work with the Inbound Marketing team and other stakeholders (Content, DemandGen, Brand, etc.) to iterate on new features and improvements to GitLab's digital marketing platform.
* Consistently ship small features and improvements to our [Slippers design system](https://gitlab.com/gitlab-com/marketing/inbound-marketing/slippers-ui)
* Help improve the overall experience of our Marketing website through improving the quality of the Frontend features.
* Help identify areas of improvements in the code base that help contribute to make it better.
* Help plan sprints and complete prioritized issues from the issue tracker.
 
## Requirements
* Knowledge of HTML, CSS, HAML and JavaScript (jQuery, Vue.js).
* Experience with responsive design and best practices.
* Knowledge of current web accessibility standards and requirements.
* Knowledge of information architecture, interaction design, and user-centered design.
* Strong knowledge in core web and browser concepts (eg. how the browser parses and constructs a web page).
* Experience with performance and optimization problems and a demonstrated ability to both diagnose and prevent these problems.
* Experience using design software to collaborate with designers and create pixel perfect web pages (eg. Figma, Sketch).
* Experience with Git in a professional/workplace environment, ideally using the GitLab product as a user or contributor.
* The ability to work in an agile, [iterative](/handbook/values/#iteration) development process and embrace feedback from many perspectives.
* Proficiency in the English language, with the capacity to communicate complex technical problems, provide clear status updates, and [achieve consensus with peers](/handbook/values/#collaboration).
* [Self-motivated and self-managing](/handbook/values/#efficiency), with great organizational skills and a positive, solution-oriented mindset.
* An interest in [our values](/handbook/values/), and working in accordance with those values.
* Prefer experience with JAMstack, Ruby, and Middleman (and/or other static site generators).
* Prefer SaaS Product company experience.
* Prefer experience working with a global or otherwise multicultural team.
* Prefer understanding of marketing concepts such as conversion, analytics, A/B testing, lead generation, buyer journeys, and search engine optimization.
 
## Levels
 
### Frontend Engineer (Intermediate)  

The Frontend Engineer (Intermediate) reports the the [Senior Manager, Digital Experience](/job-families/marketing/marketing-web-developer-designer/).
 
#### Frontend Engineer (Intermediate) Job Grade
The Frontend Engineer (Intermediate) is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
 
#### Frontend Engineer (Intermediate) Responsibilities
* Partner with team members on the simplest solutions to problems.
* Develop features and enhancements to GitLab's marketing site in a secure, well-tested, and performant way.
* Craft code that meets our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review.
 
#### Frontend Engineer (Intermediate) Requirements
* 3-5 years experience specializing in frontend development for websites and web applications.
 
### Senior Frontend Engineer  

The Senior Frontend Engineer (Intermediate) reports the the [Senior Manager, Digital Experience](/job-families/marketing/marketing-web-developer-designer/).
 
#### Senior Frontend Engineer Job Grade
The Senior Frontend Engineer is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).  
 
### Senior Frontend Engineer Responsibilities
* Work with cross functional partners, acting as a team leader.
* Advocate for improvements to Marketing website quality, security, and performance that have particular impact across your team.
* Help to define and improve our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review.
* Mentor team members in your section to help them grow in their technical responsibilities and remove blockers.
* Consistently ship moderately sized features and improvements with minimal guidance from other team members.
 
### Senior Frontend Engineer Requirements
* 6+ years experience specializing in frontend development for websites and web applications.
* Critical decision making, and knowing what will have the biggest business impact when prioritizing.
* Experience with performance and optimization problems, particularly at large scale, and a demonstrated ability to both diagnose and prevent these problems.
 
## Performance Indicators
* [Contributing to the success of Marketing's Quarterly Initiatives](/handbook/marketing/inbound-marketing/#q3-fy21-initiatives)
* [Identifying and organizing epics into executable Sprint plans](/handbook/marketing/inbound-marketing/digital-experience/#sprint-planning)
* [Successfully completing weekly Sprint tasks](/handbook/marketing/growth-marketing/brand-and-digital-design/#sprint-cycle)
* [Collaborating on identifying issues to be completed within Epics](/handbook/marketing/inbound-marketing/#epics)
 
## Career Ladder
 
The next step in the Frontend Engineer job family is not yet defined at GitLab.
 
## Hiring Process
 
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, you can find their job title on our [team page](/company/team/).
 
* Select candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 30 minute interview with our Senior Manager, Digital Experience.
* Next, candidates will be invited to schedule a 30 minute technical interview with our Fullstack Engineer and a Frontend Engineer on the Marketing team. 
* Next, candidates will be invited to schedule a 30 minute interview with the Senior Director, Growth Marketing.
* Finally, candidates will be invited to schedule a 30 minute follow up interview with the Senior Manager, Digital Experience.
 
Additional details about our process can be found on our [hiring page](/handbook/hiring/).